var baUrl="http://www.abril.com.br/barrasup/";
var baUA=new String(document.location);
var baRF=new String(document.referrer);
var baTrg="";
var baSt="";
var baEsp="";
if(typeof baTarg!="undefined") {
    if(baTarg!="_blank")baTarg="_self"
}

else baTarg="_self";
if(typeof nome_site=="undefined")nome_site="";
var stInf="recreio|recreionline_tmp|recreionline|witch|jovem|planetinha|";
var isInf=false;
if(typeof nome_site!="undefined"&&stInf.indexOf(nome_site+"|")>-1&&nome_site!="")isInf=true;
if(typeof https!="undefined")baUrl="/";
if(typeof baFrame!="undefined")baTrg="target='_blank'";
function dw(texto) {
    document.write(texto)
}

var baGaCat;
var baGaQuery;
if(typeof nome_site=="undefined"||nome_site=="") {
    baGaCat=baUA.split("/")[2];
    if(baGaCat.indexOf("www.")==0)baGaCat=baGaCat.split(".")[1];
    else baGaCat=baGaCat.split(".")[0]
}

else baGaCat=nome_site;
baGaQuery="utm_source=barra_abril&utm_medium="+baGaCat+"&utm_campaign=barra_abril_"+baGaCat;
var baDef=new Array(["Grupo Abril", "http://www.grupoabril.com.br?"], ["Abril M&iacute;dia", "http://www.grupoabril.com.br/institucional/editora-abril.shtml?"], ["Distribui&ccedil;&atilde;o", "http://www.grupoabril.com.br/institucional/distribuidora-logistica.shtml?"], ["Gr&aacute;fica", "http://grafica.abril.com.br/abril_grafica.php?"], ["Abril Educa&ccedil;&atilde;o", "http://www.abrileducacao.com.br?"]);
var baDefDir=new Array(["Assine", "http://www.assineabril.com.br/portal/home.action?origem=sr_"+baGaCat+"_barrasites&"], ["Clube", "http://www.clubedoassinante.abril.com.br?"], ["SAC", "https://www.sac.abril.com.br?"]);
baLst= {
    "ACL": ["Abril Classificados", "classificados"], "COL": ["Abril Cole&ccedil;&otilde;es", "colecoes"], "AED": ["Abril Educa&ccedil;&atilde;o", "www.abrileducacao.com.br"], "ALN": ["Abril em Londres", "www.abrilemlondres.com.br"], "GRF": ["Abril Gr&aacute;fica", "www.graficaabril.com.br"], "AJV": ["Abril Jovem", "jovem"], "AMD": ["Abril M&iacute;dia Digital", "digital"], "ASH": ["Abril Shopping", "www.abrilshopping.com.br"], "ASC": ["AbrilSac", "www.abrilsac.com.br"], "ALF": ["Alfa", "revistaalfa"], "ALM": ["Almanaque Abril", "almanaque"], "ALP": ["Alphabase", "www.alphabase.com.br"], "ANA": ["Ana Maria", "mdemulher.abril.com.br/revistas/anamaria"], "ANG": ["Anglo", "www.cursoanglo.com.br/webstander"], "ANU": ["Anuncie Abril", "publicidade"], "AES": ["Apoio Escola", "www.apoioescola.com.br"], "ACO": ["Arq. & Constru&ccedil;&atilde;o", "casa.abril.com.br/arquitetura"], "ASA": ["Assine Abril", "www.assineabril.com.br/portal/home.action?origem=sr_"+baGaCat+"_barrasites"], "ATC": ["&Aacute;tica", "www.atica.com.br"], "AVH": ["Aventuras na Hist&oacute;ria", "historia"], "BEB": ["Bebe.com.br", "bebe"], "BIZ": ["Bizz", "bizz"], "BFO": ["Boa Forma", "boaforma"], "BFU": ["Bons Fluidos", "bonsfluidos"], "BRA": ["Bravo!", "bravonline"], "CAP": ["Capricho", "capricho"], "CAR": ["Caras", "www.caras.com.br"], "CAC": ["Casa Claudia", "casa.abril.com.br/casa-claudia"], "CAS": ["Casa.com", "casa"], "CLA": ["Claudia", "claudia"], "CCB": ["Claudia Comida & Bebida", "claudia.abril.com.br/comidaebebida"], "CLK": ["Click.&agrave;.Porter", "clickaporter"], "CBA": ["ClubAlfa", "clubalfa"], "CAA": ["Clube do Assinante Abril", "www.clubedoassinante.abril.com.br"], "COM": ["Contigo!", "contigo"], "CAJ": ["Curso Abril de Jornalismo", "cursoabril.com.br"], "CPH": ["Curso e Col&eacute;gio pH", "www.ph.com.br"], "DAT": ["Datalistas", "www.datalistas.com.br"], "DIN": ["Dinap", "www.dinap.com.br/site/home"], "DIS": ["Disney", "jovem"], "EDU": ["Educar para Crescer", "educarparacrescer"], "ELE": ["Elle", "elle"], "ENM": ["Enem 2011 - Guia do Estudante", "enem2011.guiadoestudante.abril.com.br"], "EST": ["Estilo", "revistaestilo"], "EXA": ["Exame", "portalexame"], "PME": ["Exame PME", "exame.abril.com.br/pme"], "FCD": ["FC Comercial e Distribuidora", "www.chinaglia.com.br"], "FVC": ["Funda&ccedil;&atilde;o Victor Civita", "www.fvc.org.br"], "GLO": ["Gloss", "gloss"], "GRI": ["Grid - 4 Rodas", "quatrorodas.abril.com.br/grid"], "GRA": ["Grupo Abril", "www.grupoabril.com.br"], "ETB": ["Grupo ETB", "www.grupoetb.com.br"], "GQR": ["Guia 4 Rodas", "viajeaqui.abril.com.br/guia4rodas"], "GDE": ["Guia do Estudante", "guiadoestudante"], "GRD": ["Guia Rodovi&aacute;rio", "mapas.viajeaqui.abril.com.br/guiarodoviario/guia_Rodoviario_viajeaqui.aspx"], "HAB": ["Habla", "habla"], "INF": ["Info", "info"], "LOJ": ["Clube do Assinante", "www.clubedoassinante.abril.com.br"], "LOL": ["Lola", "lolamag"], "LOV": ["Loveteen", "loveteen"], "MDM": ["MdeMulher", "mdemulher"], "MOB": ["+Mob", "www.maismob.com.br"], "MNQ": ["Manequim", "manequim"], "MAX": ["M&aacute;xima", "mdemulher.abril.com.br/revistas/maxima"], "MEN": ["Men's Health", "menshealth"], "MCA": ["Minha Casa", "casa.abril.com.br/minha-casa"], "MNO": ["Minha Novela", "mdemulher.abril.com.br/revistas/minhanovela"], "MSP": ["Modaspot", "modaspot"], "MES": ["Mundo Estranho", "mundoestranho"], "MTV": ["MTV", "www.mtv.com.br"], "NAT": ["National Geographic", "viajeaqui.abril.com.br/national-geographic"], "NJV": ["NJovem", "www.njovem.com.br"], "NES": ["Nova Escola", "novaescola"], "NOV": ["Nova", "nova"], "PAS": ["Passaporte Abril", "passaporte"], "PLC": ["Placar", "placar"], "PSU": ["Planeta Sustent&aacute;vel", "planetasustentavel"], "PBY": ["Playboy", "playboy"], "POF": ["Portal do Fornecedor", "www.grupoabril.com.br/fornecedores"], "PVC": ["Pra&ccedil;a Victor Civita", "pracavictorcivita.org.br"], "PAJ": ["Pr&ecirc;mio Abril de Jornalismo", "premioabrildejornalismo"], "PAP": ["Pr&ecirc;mio Abril de Publicidade", "publiabril.com.br/premioabril"], "PAA": ["PubliAbril - Anuncie", "publicidade"], "QUA": ["Quatro Rodas", "4rodas"], "REC": ["Recreio", "recreionline"], "REL": ["Rede Elemidia", "www.elemidia.com.br"], "REF": ["Reforma ortogr&aacute;fica", "www.abril.com.br/reforma-ortografica"], "RUN": ["Runners", "runnersworld"], "SAL": ["Sa&uacute;de", "saude"], "SCI": ["Scipione", "www.scipione.com.br"], "SER": ["Ser", "www.ser.com.br"], "SOU": ["Sou + Eu!", "mdemulher.abril.com.br/revistas/soumaiseu"], "SUR": ["SuperSurf", "supersurf"], "SUP": ["Superinteressante", "super"], "TIT": ["Tititi", "mdemulher.abril.com.br/revistas/tititi"], "TRL": ["Treelog Log&iacute;stica", "www.treelog.com.br/home/home.shtml"], "VEJ": ["Veja", "veja"], "VAB": ["Veja ABC", "vejabrasil.abril.com.br/abc"], "VBH": ["Veja BH", "vejabrasil.abril.com.br/belo-horizonte"], "VBL": ["Veja Bel&eacute;m", "vejabrasil.abril.com.br/belem"], "VBA": ["Veja Brasil", "vejabrasil"], "VBR": ["Veja Bras&iacute;lia", "vejabrasil.abril.com.br/brasilia"], "VCA": ["Veja Campinas", "vejabrasil.abril.com.br/campinas"], "VCR": ["Veja Curitiba", "vejabrasil.abril.com.br/curitiba"], "VES": ["Veja ES", "vejabrasil.abril.com.br/espirito-santo"], "VFO": ["Veja Fortaleza", "vejabrasil.abril.com.br/fortaleza"], "VGO": ["Veja GO", "vejabrasil.abril.com.br/goiania"], "VLI": ["Veja Lisboa", "veja.abril.com.br/melhor_da_cidade/lisboa/index.shtml"], "VMA": ["Veja Manaus", "vejabrasil.abril.com.br/manaus"], "VNA": ["Veja Natal", "vejabrasil.abril.com.br/natal"], "VPA": ["Veja PA", "vejabrasil.abril.com.br/porto-alegre"], "VRC": ["Veja Recife", "vejabrasil.abril.com.br/recife"], "VRJ": ["Veja RJ", "vejabrasil.abril.com.br/rio-de-janeiro"], "VSL": ["Veja Salvador", "vejabrasil.abril.com.br/salvador"], "VSC": ["Veja SC", "vejabrasil.abril.com.br/santa-catarina"], "VSP": ["Veja SP", "vejasp"], "VVP": ["Veja Vale do Para&iacute;ba", "vejabrasil.abril.com.br/vale-do-paraiba"], "VET": ["Viagem e Turismo", "viajeaqui.abril.com.br/vt"], "VAQ": ["viajeaqui", "viajeaqui"], "VID": ["Vida Simples", "vidasimples"], "VIP": ["VIP", "vip"], "VIV": ["Viva Mais!", "mdemulher.abril.com.br/revistas/vivamais/"], "VRH": ["Voc&ecirc; RH", "revistavocerh"], "VSA": ["Voc&ecirc; S/A", "vocesa"], "WIT": ["Witch", "jovem"], "WOM": ["Women's Health", "revistawomenshealth"], "BPT": ["Brasil Post", "www.brasilpost.com.br"], "ESP": ["Meu Espelho", "www.meuespelho.com.br"]
}

;
for(i in baLst) {
    baLst[i][1]="http://"+baLst[i][1];
    if(baLst[i][1].indexOf(".")==-1)baLst[i][1]+=".abril.com.br";
    if(baLst[i][1].indexOf("?")==-1)baLst[i][1]+="?";
    else baLst[i][1]+="&"
}

dw("<st"+'yle type="text/css">');
dw("#barra-abril{text-align:center;width:100%;min-width:1004px;height:30px;background-color:#fff;border-bottom:1px solid #d8d9d3}");
dw('#barra-abril{width: expression(document.body.clientWidth < 1005 ? "1004px" : "auto" );}');
dw("#barra-abril #baContnr{text-align:left;width:1004px;padding:0;margin:0 auto}");
dw("#barra-abril #baContnr ul{margin:0;padding:0;list-style:none outside none}");
dw("#barra-abril #baContnr #baMenu{width:1004px;height:30px;clear:both}");
dw('#barra-abril #baContnr #baMenu.baBorda{background:url("'+baUrl+'s/i/baIcons.gif") repeat-x scroll 0px -97px transparent}');
dw('#barra-abril #baContnr #baMenu #baArv{height:30px;width:25px;float:left;margin:0 9px 0;background:url("'+baUrl+'s/i/baIcons.gif") no-repeat scroll 0 -3px transparent}');
dw("#barra-abril #baContnr #baMenu .baSites{margin:7px 0 0;display:block;float:left}");
dw("#barra-abril #baContnr #baMenu .baSites.baInsDir{float:right}");
dw("#barra-abril #baContnr #baMenu .baSites li.baNenhum{background:none;border-width:0 0 0 1px;border-style:solid;border-color:#333}");
dw("#barra-abril #baContnr #baMenu .baSites li.baNenhum.baInsOne{border:none;padding:0 9px 0 5px}");
dw('#barra-abril #baContnr #baMenu .baSites li{background:url("'+baUrl+'s/i/baIcons.gif") no-repeat scroll -11px -47px transparent;padding:0 9px 0 13px;margin:0;font-family:arial;font-size:12px;font-weight:bold;color:#333;float:left;display:block}');
dw("#barra-abril #baContnr #baMenu .baSites li a{cursor:pointer;text-decoration:none;color:#333}");
dw("#barra-abril #baContnr #baMenu .baSites li #baNoBrdr #baMais{cursor:pointer;text-decoration:none;color:#008457;display:block;float:left;height:25px}");
dw("#barra-abril #baContnr #baMenu .baSites li #baNoBrdr{height:25px;float:left}");
dw("#barra-abril #baContnr #baMenu .baSites li #baNoBrdr.baNoBord{background-color:#fff}");
dw("#barra-abril #baContnr #baMenu #baGrupo{float:right;height:18px;margin:5px 0 0 5px;padding:2px 0 0 15px;width:94px;font-family:arial;font-size:12px;color:#333;text-decoration:none;border-width:0 0 0 1px;border-style:solid;border-color:#333}");
dw("#barra-abril #baContnr #baMenu #baGrupo span.baGrpAbr{font-weight:bold}");
dw("#barra-abril #baContnr #baLista{z-index:2147483583;position:absolute;display:none;width:1001px;background-color:#fff;border-style:none solid solid;border-color:#d8d9d3;border-width:1px}");
dw("#barra-abril #baContnr #baLista ul{margin:12px 0 0 38px;float:left;display:block;padding:0 0 18px;text-align:left}");
dw("#barra-abril #baContnr #baLista ul li{margin:4px 0 0 0;line-height:13px;font-family:arial;font-size:13px;font-weight:bold;color:#333;display:block}");
dw("#barra-abril #baContnr #baLista ul li a{text-decoration:none;color:#333}");
dw("</st"+"yle>");
dw('<div id="barra-abril">');
dw('<div id="baContnr">');
dw('<div id="baMenu">');
dw('<a id="baArv" href="http://www.abril.com.br'+"?"+baGaQuery+'" target="'+baTarg+'"></a>');
if(typeof baSites!="undefined") {
    dw('<ul class="baSites">');
    for(i=0;
    i<baSites.length;
    i++)if(i==0)dw('<li class="baNenhum"><a href="'+baLst[baSites[i]][1]+baGaQuery+'" title="'+baLst[baSites[i]][0]+'" target="'+baTarg+'">'+baLst[baSites[i]][0]+"</a></li>");
    else dw('<li><a href="'+baLst[baSites[i]][1]+baGaQuery+'" title="'+baLst[baSites[i]][0]+'" target="'+baTarg+'">'+baLst[baSites[i]][0]+"</a></li>");
    dw('<li><div id="baNoBrdr"><a id="baMais" href="http://www.abril.com.br/revistas?'+baGaQuery+'" title="Revistas e sites" target="'+ baTarg+'">Revistas e sites</a></div></li>');
    dw("</ul>");
    dw('<ul class="baSites baInsDir">');
    if(nome_site!="portal")for(i=0;
    i<baDefDir.length;
    i++)if(i==0)dw('<li class="baNenhum baInsOne"><a href="'+baDefDir[i][1]+baGaQuery+'" title="'+baDefDir[i][0]+'" target="'+baTarg+'">'+baDefDir[i][0]+"</a></li>");
    else dw('<li><a href="'+baDefDir[i][1]+baGaQuery+'" title="'+baDefDir[i][0]+'" target="'+baTarg+'">'+baDefDir[i][0]+"</a></li>");
    dw('<li class="baNenhum"><a href="http://www.grupoabril.com.br?'+baGaQuery+ '" title="Grupo Abril" target="'+baTarg+'">Grupo Abril</a></li>');
    dw("</ul>")
}

else {
    dw('<ul class="baSites">');
    dw('<li class="baNenhum baInsOne"><a href="http://www.abril.com.br'+"?"+baGaQuery+'" target="'+baTarg+'">Abril.com</a></li>');
    dw('<li><div id="baNoBrdr"><a id="baMais" href="http://www.abril.com.br/revistas?'+baGaQuery+'" title="Revistas e sites" target="'+baTarg+'">Revistas e sites</a></div></li>');
    dw("</ul>");
    dw('<ul class="baSites baInsDir">');
    for(i=0;
    i<baDefDir.length;
    i++)if(i==0)dw('<li class="baNenhum"><a href="'+ baDefDir[i][1]+baGaQuery+'" title="'+baDefDir[i][0]+'" target="'+baTarg+'">'+baDefDir[i][0]+"</a></li>");
    else dw('<li><a href="'+baDefDir[i][1]+baGaQuery+'" title="'+baDefDir[i][0]+'" target="'+baTarg+'">'+baDefDir[i][0]+"</a></li>");
    dw("</ul>");
    dw('<ul class="baSites baInsDir">');
    for(i=0;
    i<baDef.length;
    i++)if(i==0)dw('<li class="baNenhum baInsOne"><a href="'+baDef[i][1]+baGaQuery+'" title="'+baDef[i][0]+'" target="'+baTarg+'">'+baDef[i][0]+"</a></li>");
    else dw('<li><a href="'+baDef[i][1]+baGaQuery+ '" title="'+baDef[i][0]+'" target="'+baTarg+'">'+baDef[i][0]+"</a></li>");
    dw("</ul>")
}

dw("</div>");
dw("</div>");
dw("</div>");