//Metodos de Eventos (handlers)
function montaAbertura(e){
	var logo = $("HEADER").find("H2");
	var logoA = logo.css("opacity");
	logo.css("opacity", "0");

	var btGE = $("HEADER").find("#bt-acesso-ao-ge");
	var btGEY = btGE.css("top");
	btGE.css({top:"-20px"});

	var btRedes = $("HEADER").find("#bt-ja-fiz");
	var btRedesA = btRedes.css("opacity");
	btRedes.css("opacity", "0");

	var btSalvar = $("HEADER").find("#bt-salvar");
	var btSalvarA = btSalvar.css("opacity");
	btSalvar.css("opacity", "0");

	var tit = e.elm.find("H1");
	var titA = tit.css("opacity");
	tit.css("opacity", "0");

	var bold = e.elm.find("STRONG");
	var boldA = bold.css("opacity");
	bold.css("opacity", "0");

	var txt = e.elm.find("P");
	var txtA = txt.css("opacity");
	txt.css("opacity", "0");

	var bt1 = e.elm.find("A");
	var bt1A = bt1.css("opacity");
	bt1.css("opacity", "0");

	var btAjuda = $("FOOTER").find("SPAN");
	var btAjudaA = btAjuda.css("opacity");
	btAjuda.css("opacity", "0");

	var copy = $("FOOTER").find("P");
	var copyA = copy.css("opacity");
	copy.css({opacity:"0"});
	
	TweenLite.delayedCall(.5, function(){
		$("HEADER").show();
		$("FOOTER").show();
		e.elm.show();

		TweenLite.to(logo, .7, {alpha:logoA, ease:Quint.easeOut});
		TweenLite.to(tit, 1, {alpha:titA, delay:.2, ease:Quint.easeOut});
		TweenLite.to(bold, .7, {alpha:boldA, delay:.3, ease:Quint.easeOut});
		TweenLite.to(txt, .7, {alpha:txtA, delay:.4, ease:Quint.easeOut});
		TweenLite.to(bt1, .7, {alpha:bt1A, delay:.5, ease:Quint.easeOut});
		TweenLite.to(btRedes, .7, {alpha:btRedesA, delay:.5, ease:Quint.easeOut});
		TweenLite.to(btAjuda, .7, {alpha:btAjudaA, delay:.5, ease:Quint.easeOut});
		TweenLite.to(btGE, .5, {top:btGEY, delay:.7, ease:Quint.easeOut});
		TweenLite.to(copy, .5, {alpha:copyA, delay:.7, ease:Quint.easeOut, onComplete:function(){
			e.nav.clique = true;
			e.elm.show();
		}});
	});
}

function desmontaAbertura(e){
	
	var tit = e.elm.find("H1");
	var titA = tit.css("opacity");
	var titY = tit.css("top");

	var bold = e.elm.find("STRONG");
	var boldX = bold.css("left");

	var txt = e.elm.find("P");
	var txtX = txt.css("left");

	var bt1 = e.elm.find("A");
	var bt1A = bt1.css("opacity");

	var btAjuda = $("FOOTER").find("SPAN");
	var btAjudaA = btAjuda.css("opacity");

	var btRedes = $("HEADER").find("#bt-ja-fiz");
	var btRedesA = btRedes.css("opacity");
		
	TweenLite.to(bold, .4, {left:"-2000px", ease:Back.easeIn});
	TweenLite.to(txt, .4, {left:"-2000px", delay:.1, ease:Quint.easeIn});
	TweenLite.to(bt1, .7, {alpha:0, delay:.1, ease:Quint.easeOut});
	TweenLite.to(btAjuda, .7, {autoAlpha:0, delay:.1, ease:Quint.easeOut});
	TweenLite.to(btRedes, .7, {autoAlpha:0, delay:.1, ease:Quint.easeOut});
	TweenLite.to(tit, .4, {top:"-300px", alpha:0, delay:.2, ease:Back.easeIn, onComplete:function(){
		
		bold.css({left:boldX});
		txt.css({left:txtX});
		bt1.css("opacity", bt1A);
		btAjuda.css("opacity", btAjudaA);
		//btRedes.css("opacity", btRedesA);
		tit.css({top:titY, opacity:titA});

		e.elm.hide();
		e.nav.clique = true;
	}});
}

function montaQuiz(e){
	var btSalvar = $("HEADER").find("#bt-salvar");
	var btSalvarA = btSalvar.css("opacity");
	btSalvar.css("opacity", "0");


	TweenLite.delayedCall(.8, function(){
		e.elm.show();
		TweenLite.set(e.elm.selector, {autoAlpha:"0"});
		TweenLite.to(btSalvar, 1, {autoAlpha:"1", ease:Quint.easeInOut});
		TweenLite.to(e.elm.selector, 1, {autoAlpha:"1", ease:Quint.easeInOut, onComplete:function(){
			e.nav.clique = true;
			Quiz.Iniciar();
		}});
	});
}

function desmontaQuiz(e){
	e.nav.clique = false;
	var obj = Quiz;

	var frase = e.elm.find("BLOCKQUOTE");
	var aside = e.elm.find("ASIDE");
	var span = e.elm.find("SPAN");
	TweenLite.to(frase, .5, {left:-500, autoAlpha:0, ease:Back.EaseIn});
	TweenLite.to(aside, .5, {left:1000, autoAlpha:0, ease:Back.EaseIn});
	TweenLite.to(span, .5, {autoAlpha:0, ease:Back.EaseIn});
	
	if(obj.canvas){
		obj.canvas.Sair(function(){
			e.elm.hide();
			e.nav.clique = true;
		});
	}
	else{
		e.elm.hide();
		e.nav.clique = true;
	}
}

function montaFinal(e){
	
	var bloco = e.elm.find("BLOCKQUOTE").eq(0);
	bloco.css({"opacity":"0"});

	var aside = e.elm.find("ASIDE");
	var asideX = aside.css("left");
	var asideA = aside.css("opacity");
	aside.css({"opacity":"0", "left":"-500px"});

	var div = e.elm.find("DIV").eq(0);
	var divX = div.css("right");
	div.css({"opacity":"0", "right":"-500px"});

	var block = div.find("BLOCKQUOTE").eq(1);
	block.css({"opacity":"0"});
	
	var time = (Quiz.canvas)? 5.3 : .2;//tempo de delay para montar a tela varia de acordo com os dados salvos. Se o quiz já está completo, monta a tela final direto;

	TweenLite.delayedCall(time, function(){
		e.elm.show();
		TweenLite.to(bloco, .7, {autoAlpha:1, ease:Quint.easeIn});
		TweenLite.to(aside, 1, {autoAlpha:asideA, left:asideX, delay:.2, ease:Quint.easeIn});
		TweenLite.to(div, 1, {autoAlpha:1, right:divX, delay:.2, ease:Quint.easeIn});
		TweenLite.to(div, 1, {height:"367px", delay:1.2, ease:Bounce.easeOut, onComplete:function(){
			e.nav.clique = true;
		}});
		TweenLite.to(block, .5, {autoAlpha:1, delay:1.5, ease:Quint.easeIn});
	})
}

function desmontaFinal(e){
	e.nav.clique = false;
	e.elm.hide();
	e.nav.clique = true;
}