//Objeto que controla a navegação entre as templates
var objNavegador = function(){
	this.telaAtual = null;
	this.telaAnterior = null;
	this.arrTelas = [];
	this.clique = true;//controle para evitar multiplos cliques
	
	if(arrConteudo){
		var i;
		for(i=0; i<arrConteudo.length; i++){
			arrConteudo[i].id = i;
			arrConteudo[i].navegador = this;
			this.addTela(arrConteudo[i]);
		}
	}
}
objNavegador.prototype.addTela = function(arr){
	var obj = new objTela(arr);
	this.arrTelas.push(obj);
}
objNavegador.prototype.goTo = function(id){
	if(this.clique){
		this.clique = false;
		this.telaAnterior = this.telaAtual;
		this.telaAtual = id;
		if(this.arrTelas[this.telaAnterior]){
			this.arrTelas[this.telaAnterior].Fechar();
		}
		if(this.arrTelas[this.telaAtual]){
			this.arrTelas[this.telaAtual].Abrir();
		}
	}
}
objNavegador.prototype.Proximo = function(){
	this.goTo(this.telaAtual + 1);
}
objNavegador.prototype.Anterior = function(){
	this.goTo(this.telaAtual - 1);
}

//Objeto modelo para telas
var objTela = function(args){
	if(!this.id){
		this.id = (args.id != null)? args.id : this.id;
	}
	
	if(this.id != null){
		this.tpl = (args.tpl)? args.tpl : this.tpl;
		this.tit = (args.tit)? args.tit : this.tit;

		this.navegador = args.navegador;
		this.elm = (!this.elm)? this.Renderizar() : this.elm;

		//dispara evento "onAddedX" onde X = ao id do objTela
		$.event.trigger({type:"onAdded"+this.id, id:this.id, nav:this.navegador, elm:this.elm, message:"onAdded"+this.id+" disparado", time:new Date()});
	}
}
objTela.prototype.Renderizar = function(){//cria uma copia do elemento HTML e insere no final do body, gerando um id = "tela_X" onde X equivale ao id de arrConteudo
	var elm = $("."+this.tpl);
	elm.attr("id", "tela_" + this.id); //determina o id "tela_X"
	
	elm.find("#bt-comecar").off().click(function(){
		Navegador.Proximo();
		track('["_trackEvent", "Home", "Clicou em Começar"]');
	});

	
	$("HEADER #bt-ja-fiz").off().click(function(e){
		Navegador.Proximo();
		Quiz.IniciarSalvo();
	});

	$("HEADER #bt-acesso-ao-ge").off().click(function(e){
		track('["_trackEvent", "Saiu", "Topo", "Acesso ao GE"]');
		window.location = "http://guiadoestudante.abril.com.br";
	});

	$("FOOTER SPAN A").off().click(function(){
		$(".pop").find("STRONG").html(arrPop[0].tit);
		
		$(".pop").find("ARTICLE #mCSB_1_container")
		.html(arrPop[0].txt)
		.css("top", 0);

		$(".pop").find("ARTICLE .mCSB_dragger")
		.css("top", 0);

		openPop(".pop");
		//
		track('["_trackPageview", "home/saibasobreoteste"]');
	})

	$(".pop A").off().click(function(){
		closePop(".pop");
	})
	
	return $("#tela_" + this.id);
}
objTela.prototype.Abrir = function(){
	//dispara evento "onOpenX" onde X = ao id do objTela
	$.event.trigger({type:"onOpen"+this.id, id:this.id, nav:this.navegador, elm:this.elm, message:"onOpen"+this.id+" disparado", time:new Date()});
}
objTela.prototype.Fechar = function(){
	//dispara evento "onCloseX" onde X = ao id do objTela
	$.event.trigger({type:"onClose"+this.id, id:this.id, nav:this.navegador, elm:this.elm, message:"onClose"+this.id+" disparado", time:new Date()});
}


//objetos customizados
// QUIZZ
var objQuiz = function(){
	this.salvo = false;
	this.nivel = 0;
	this.canvas = null;
	this.roleta = null;
	this.arr_ico = [];
}
objQuiz.prototype.Iniciar = function(){
	var objTela = Navegador.arrTelas[Navegador.telaAtual];
	var elm = objTela.elm;
	var obj = this;

	if(obj.salvo && $.cookie("maquinadeprofissoes")){
		var dados = JSON.parse($.cookie("maquinadeprofissoes"));
		obj.nivel = (dados.nivel)? dados.nivel : 0;
		Frase.frase = (dados.frase)? dados.frase : 0;
		Frase.count = (dados.count)? dados.count-1 : 0;
		Personalidade.nivel = (dados.nivel)? dados.nivel-1 : 0;
		Personalidade.arr_tipo = (dados.arr_tipo)? dados.arr_tipo : [];
		Personalidade.arr_resposta = (dados.arr_resposta)? dados.arr_resposta : [];
		Personalidade.resultado = (dados.resultado)? dados.resultado : null;
	}

	if(obj.nivel < arrPergunta.length){
		//constroi canvas principal
		obj.canvas = new objCanvas();
		obj.canvas.Renderizar();

		//constroi canvas lateral referente a roleta de personalidades
		obj.roleta = new objRoleta();
		obj.roleta.Renderizar();

		//
		obj.Renderizar();
		Marcador.Atualizar();
		Frase.Atualizar();
		obj.canvas.Iniciar();

		if(obj.salvo){
			Frase.Renderizar();
		}
	}
	else{
		Final.Renderizar();
		Navegador.clique = true;
		Navegador.Proximo();
	}
}
objQuiz.prototype.IniciarSalvo = function(){
	var obj = this;
	obj.salvo = true;
	track('["_trackEvent", "Home", "Clicou em Começar jogo salvo"]')
}
objQuiz.prototype.Renderizar = function(){
	var objTela = Navegador.arrTelas[Navegador.telaAtual];
	var elm = objTela.elm;
	var obj = this;
	var pergunta = arrPergunta[this.nivel];
	var str_nivel = (this.nivel < 9)? "0" + (this.nivel +1): (this.nivel +1);
	
	elm.find("DIV STRONG").html(pergunta.txt);
	elm.find("DIV A LABEL").attr('unselectable', 'on').css('user-select', 'none').on('selectstart', false);
	elm.find("DIV A LABEL").eq(0).html(pergunta.optA.replace(/./, function(m){ return m.toUpperCase()}));
	elm.find("DIV A LABEL").eq(1).html(pergunta.optB.replace(/./, function(m){ return m.toUpperCase()}));

	elm.find("DIV A").eq(0).off().click(function(e){
		if(Navegador.clique){
			obj.Avancar(0);
		}
	});

	elm.find("DIV A").eq(1).off().click(function(e){
		if(Navegador.clique){
			obj.Avancar(1);
		}
	});

	$("HEADER #bt-salvar").off().click(function(e){
		obj.Salvar();
	});

	track('["_trackPageview", "teste/'+str_nivel+'"]');
}
objQuiz.prototype.Avancar = function(opcao){
	var objTela = Navegador.arrTelas[Navegador.telaAtual];
	var elm = objTela.elm;
	var obj = this;
	var txt = elm.find("DIV STRONG");
	var btA = (opcao) ? elm.find("DIV A").eq(1) : elm.find("DIV A").eq(0);
	var btB = (opcao) ? elm.find("DIV A").eq(0) : elm.find("DIV A").eq(1);
	var abaAnterior = obj.canvas.arr_aba[obj.nivel];
	var abaAtual = obj.canvas.arr_aba[obj.nivel + 1];

	Personalidade.Computar(opcao);

	obj.nivel ++;
	Navegador.clique = false;

	TweenLite.to(txt, .7, {alpha:0, ease:Quint.easeInOut})
	TweenLite.to(btA, .5, {alpha:-1, left:"400px", ease:Back.easeIn})
	TweenLite.to(btB, .5, {alpha:-1, left:"400px", delay:.2, ease:Back.easeIn, onComplete:function(){
		if(obj.nivel == arrPergunta.length){
			Final.Renderizar();
			Navegador.clique = true;
			Navegador.Proximo();
		}
		else{
			TweenLite.set(btA, {left:"-200px"});
			TweenLite.set(btB, {left:"-200px"});
			obj.Renderizar();
			TweenLite.to(txt, .7, {alpha:1, ease:Quint.easeInOut})
			TweenLite.to(btA, .5, {alpha:1, left:"140px", ease:Quint.easeInOut});
			TweenLite.to(btB, .5, {alpha:1, left:"140px", delay:.2, ease:Quint.easeInOut});
			Navegador.clique = true;

			Frase.Atualizar();
			Marcador.Atualizar();
			abaAtual.Ativar();
			abaAnterior.Desativar();
			Personalidade.Atualizar();
		}
	}});	
}
objQuiz.prototype.Salvar = function(){
	
	var obj = {
		"nivel": Quiz.nivel,
		"arr_tipo": Personalidade.arr_tipo,
		"arr_resposta": Personalidade.arr_resposta,
		"resultado": Personalidade.resultado,
		"frase": Frase.frase,
		"count": Frase.count
	}

	var Salvar = function(){
		track('["_trackEvent", "Teste", "Clicou para salvar o resultado"]');
		$.cookie("maquinadeprofissoes", JSON.stringify(obj), {expires:99999});
	}

	if($.cookie("maquinadeprofissoes")){
		
		var pop = confirm("Você já tem um teste salvo, deseja substituí-lo por este?");
		if(pop){
			Salvar();
		}
		//console.log($.cookie("maquinadeprofissoes"));
	}
	else{
		Salvar();
	}
}


//CANVAS PRINCIPAL
var objCanvas = function(){
	this.stage;
	this.arr_aba = [];
	this.circulo;
	this.marcador;
}
objCanvas.prototype.Renderizar = function(){
	var obj = this;

	//constroi o canvas central
	obj.stage = new Kinetic.Stage({
        container: 'canvas',
        width: 490,
        height: 490
    });

    var layer = new Kinetic.Layer();
	for(i=0; i<arrPergunta.length; i++){
		var aba = new objAba(i);
		layer.add(aba.Renderizar());
		obj.arr_aba.push(aba);
	}

	this.circulo = new Kinetic.Circle({
		radius: 168,
		fill: '#FFFFFF',
		x:($('.quiz #canvas').width()/2),
		y:($('.quiz #canvas').height()/2),
		scaleX: 0,
		scaleY:0,
		id:"circulo"
	});
	layer.add(this.circulo);

	this.marcador = new Kinetic.Path({
		id: "marcador",
		x: $('.quiz #canvas').width()/2,
		y: 100,
		opacity:0,
		data: 'M0.15 -14.6Q23.55 -14.4 44.15 -8.7 59.9 -4.3 73.1 3.05 82.3 8.2 80.75 11.3 80.25 12.25 78.75 12.9L77.35 13.35 0.15 13.15 -0.15 13.15 -77.35 13.35 -78.75 12.9Q-80.25 12.25 -80.75 11.3 -82.3 8.2 -73.1 3.05 -60.25 -4.15 -43.35 -8.75 -22.35 -14.45 -0.15 -14.6Z',
		fill: 'red'
	});
	layer.add(this.marcador);

	obj.stage.add(layer);
}
objCanvas.prototype.Iniciar = function(){
	var obj = this;

	//anima as abas
	for(i=0; i<obj.arr_aba.length; i++){
		var duration = Math.random() * 3.5 + .1;
		var t = new Kinetic.Tween({
			node: obj.arr_aba[i].elm,
			scaleX:.8,
			scaleY:.8,
			opacity: .4,
			duration: duration,
			easing: Kinetic.Easings.BackEaseOut
		}).play();
	}

	//anima o circulo central
	TweenLite.delayedCall(1, function(){
		var t = new Kinetic.Tween({
			node: obj.circulo,
			scaleX:1,
			scaleY:1,
			duration: .7,
			easing: Kinetic.Easings.BackEaseOut
		}).play();
	});

	//anima o marcador
	TweenLite.delayedCall(2, function(){
		var t = new Kinetic.Tween({
			node: obj.marcador,
			opacity:1,
			duration: 1,
			easing: Kinetic.Easings.EaseIn
		}).play();
	});

	//anima o conteudo
	TweenLite.to(".quiz DIV SPAN", .5, {alpha:1, delay:2.1, ease:Linear.EaseIn});
	TweenLite.to(".quiz DIV STRONG", .5, {alpha:1, delay:2.3, ease:Linear.EaseIn});
	TweenLite.to(".quiz DIV A", .5, {alpha:1, delay:2.5, ease:Linear.EaseIn});

	//ativa a primeira questao
	TweenLite.delayedCall(3.5, function(){
		for(i=0; i<Quiz.nivel; i++){
			obj.arr_aba[i].Desativar();
		}

		obj.arr_aba[Quiz.nivel].Ativar();
	});
}
objCanvas.prototype.Sair = function(fn){
	var obj = this;

	//desativa a primeira questao
	obj.arr_aba[Quiz.nivel-1].Desativar();

	//anima o marcador
	TweenLite.delayedCall(.5, function(){
		var t = new Kinetic.Tween({
			node: obj.marcador,
			opacity:0,
			duration: 1,
			easing: Kinetic.Easings.EaseIn
		}).play();
	});

	//anima o circulo central
	TweenLite.delayedCall(1, function(){
		var t = new Kinetic.Tween({
			node: obj.circulo,
			scaleX:0,
			scaleY:0,
			duration: 1,
			easing: Kinetic.Easings.BackEaseIn
		}).play();
	});

	//anima as abas
	TweenLite.delayedCall(1.5, function(){
		for(i=0; i<obj.arr_aba.length; i++){
			var duration = Math.random() * 4 + .1;
			var t = new Kinetic.Tween({
				node: obj.arr_aba[i].elm,
				scaleX:0,
				scaleY:0,
				opacity: 0,
				duration: duration,
				easing: Kinetic.Easings.BackEaseInOut
			}).play();
		}
	});

	//chama o metodo de callback
	TweenLite.delayedCall(5.5, fn);
}


//PERSONALIDADE
var objPersonalidade = function(){
	this.nivel = 0;
	this.arr_tipo = [];
	this.bin_personalidade = {
		"0110": 1,
		"0111": 2,
		"0100": 3,
		"0101": 4,
		"0010": 5,
		"0011": 6,
		"0000": 7,
		"0001": 8,
		"1110": 9,
		"1111": 10,
		"1100": 11,
		"1101": 12,
		"1010": 13,
		"1011": 14,
		"1000": 15,
		"1001": 16
	};
	this.arr_resposta = [];
	this.resultado = null;
}
objPersonalidade.prototype.Computar = function(opcao){
	var obj = this;
	obj.nivel = Quiz.nivel;

	//grava a resposta atrelada a cada questão (caso seja necessário salvar);
	obj.arr_resposta[obj.nivel] = arrPergunta[obj.nivel][opcao?"resB":"resA"];

	//incrementa o valor da caracteristica escolhida em outra variavel;
	if(!obj.arr_tipo[arrPergunta[obj.nivel].tipo]){
		obj.arr_tipo[arrPergunta[obj.nivel].tipo] = [0,0];
	}
	obj.arr_tipo[arrPergunta[obj.nivel].tipo][opcao] ++;
}
objPersonalidade.prototype.Atualizar = function(){
	var obj = this;
	var bin = "";
	
	for(i=1; i<5; i++){
		var res = "0"
		if(obj.arr_tipo[i]){
			var optA = obj.arr_tipo[i][0];
			var optB = obj.arr_tipo[i][1];
			res = (optA >= optB)? "0" : "1";
		}
		bin += res;
	}	

	obj.resultado = this.bin_personalidade[bin];
	console.log(arrPersonalidade[obj.resultado - 1].name);

	var ico = Quiz.roleta.arr_ico[obj.resultado - 1]
	ico.Ativar();
}

//FRASE
var objFrase = function(){
	this.frase = 0;
	this.count = 0;
	this.intervalo = 10;
}
objFrase.prototype.Atualizar = function(){
	var objTela = Navegador.arrTelas[Navegador.telaAtual];
	var elm = objTela.elm;
	var obj = this;

	if(obj.count == obj.intervalo){
		obj.count = 0;
		obj.frase ++;
	}

	if(obj.count == 0){
		obj.Renderizar()
	}
	obj.count ++;
}
objFrase.prototype.Renderizar = function(){
	var objTela = Navegador.arrTelas[Navegador.telaAtual];
	var elm = objTela.elm;
	var obj = this;

	var T = elm.find("BLOCKQUOTE").css("top");

	TweenLite.to(elm.find("BLOCKQUOTE"), .4, {autoAlpha:0, top:"210px", ease:Quint.easeOut, onComplete:function(){
		
		elm.find("BLOCKQUOTE").css({top:"180px"});
		elm.find("BLOCKQUOTE STRONG").html(arrFrase[obj.frase].tit);
		elm.find("BLOCKQUOTE P").html(arrFrase[obj.frase].txt);

		TweenLite.to(elm.find("BLOCKQUOTE"), .4, {autoAlpha:1, top:T, ease:Quint.easeOut});
	}});
}

//Marcador
var objMarcador = function(){}
objMarcador.prototype.Atualizar = function(){
	var objTela = Navegador.arrTelas[Navegador.telaAtual];
	var elm = objTela.elm;
	var str_nivel = (Quiz.nivel < 9)? "0" + (Quiz.nivel +1): (Quiz.nivel +1);
	var limite = arrPergunta.length;
	var cor = circuloCromatico(Quiz.nivel, arrPergunta.length);
	var marcador = Quiz.canvas.stage.find('#marcador')[0];

	elm.find("DIV SPAN").html(str_nivel + "/" + limite);

	marcador.fill(cor);
}

// ABAS
var objAba = function(id){
	this.id = id;
	this.ativo = false; 
	this.str_nivel = (id < 9)? "0" + (id +1): (id +1);
	this.elm = null;
	this.cor = circuloCromatico(this.id, arrPergunta.length);
	this.anglo = id * (360/arrPergunta.length);
	this.tween;
	this.Renderizar();
}
objAba.prototype.Renderizar = function(){
	var obj = this;
	var X = ($('.quiz #canvas').width()/2);
	var Y = $('.quiz #canvas').height()/2;

	var shape = new Kinetic.Shape({
		id: "aba-" + obj.id,
		x: -11,
		y: 6 - Y, 
		fill: obj.cor,
		// a Kinetic.Canvas renderer is passed into the drawFunc function
		drawFunc: function(context) {
			context.beginPath();
			context.moveTo(0, 10);
			context.bezierCurveTo(0, 10, 2, 1, 9, 1);
			context.bezierCurveTo(14, 1, 19, 1, 22, 10);
			context.lineTo(19, 80);
			context.lineTo(3, 80);
			context.closePath();
			context.fillStrokeShape(this);
		}
	});

	var bullet = new Kinetic.Circle({
		id:"bullet-"+obj.id,
		radius:4,
		fill: "#FFFFFF",
		x: 0,
		y: 16 - Y,
		scaleX: 0,
		scaleY: 0
	});

	var text = new Kinetic.Text({
        id:"txt-"+obj.id,
        x: -11,
        y: 15 - Y,
        width: 22,
        height: 22,
        align:"center",
        fontFamily: 'solexboldregular',
        fontSize: 15,
        text: obj.str_nivel,
        fill: '#FFF',
        opacity: 0
    });

	var grupo = new Kinetic.Group({
		id:"grupo-"+obj.id,
		x: X,
		y: Y,
		opacity:0,
		scaleX:0,
		scaleY:0,
		rotation:obj.anglo
	});
	grupo.add(shape, bullet, text)

	obj.elm = grupo;
	return grupo;
}
objAba.prototype.Ativar = function(){
	var obj = this;
	var id = obj.id;
	var ativo = this.ativo;
	var shape = Quiz.canvas.stage.find('#grupo-'+id)[0];
	var text = shape.find("#txt-"+id)[0];

	if (obj.tween) {
		obj.tween.destroy();  
	}

	obj.tween = new Kinetic.Tween({
		node: shape, 
		duration: .5,
		scaleX: 1,
		scaleY: 1,
		opacity:1,
		easing: Kinetic.Easings.BackEaseOut
	}).play();

	var t = new Kinetic.Tween({
		node:text,
		duration:.5,
		opacity:1,
		easing: Kinetic.Easings.BackEaseOut
	}).play();
}
objAba.prototype.Desativar = function(){
	var obj = this;
	var id = this.id;
	var ativo = this.ativo;
	var shape = Quiz.canvas.stage.find('#grupo-'+id)[0];
	var bullet = shape.find('#bullet-'+id)[0];
	var text = shape.find("#txt-"+id)[0];

	if(Quiz.nivel > 0){
		if (obj.tween) {
			obj.tween.destroy();  
		}

		var t = new Kinetic.Tween({
			node:text,
			duration:.5,
			opacity:0,
			easing: Kinetic.Easings.EaseOut
		}).play();

		obj.tween = new Kinetic.Tween({
			node: shape, 
			duration: .3,
			scaleX: .8,
			scaleY: .8,
			opacity:1,
			easing: Kinetic.Easings.EaseIn,
			onFinish: function() {
		    	var b = new Kinetic.Tween({
		    		node:bullet,
		    		duration:.5,
		    		scaleX:1,
		    		scaleY:1,
		    		easing: Kinetic.Easings.BackEaseOut
		    	}).play();
		    }
		}).play();
	}
}

//ROLETA
var objRoleta = function(){
	this.stage;
	this.arr_ico = [];
}
objRoleta.prototype.Renderizar = function(){
	var obj = this;

	//constroi o canvas lateral referente a roleta de personalidades
	var rW = $('.quiz #roleta').width();
	var rH = $('.quiz #roleta').height();

	obj.stage = new Kinetic.Stage({
        container: 'roleta',
        width: rW,
        height: rH
    });

    var layer = new Kinetic.Layer({
    	id:"layer-ico",
    	width:rW,
    	height: rH,
    	x: rW/2,
        y: rH + (rW/-2)
    })
	
	var r = rW/2;
	var s = 32;
	var circulo = new Kinetic.Circle({
		radius: r - (s/2),
		fill: '#FFFFFF',
		x:0,
		y:0,
		stroke: '#C0C0C0',
        strokeWidth: s,
		id:"circulo"
	});
	layer.add(circulo);

	for(i=0; i<arrPersonalidade.length; i++){
		var ico = new objIco(i);
		layer.add(ico.Renderizar());
		obj.arr_ico.push(ico);
	} 

	var layerDest = new Kinetic.Layer({
    	id:"layer-dest",
    	width:103,
    	height: 50,
    	x: rW/2,
        y: 52
    });

    var img;
	var imageObj = new Image();
	imageObj.onload = function() {
		img = new Kinetic.Image({
			id:"img-dest",
			image: imageObj,
			x:-51,
			y:0
		});
		layerDest.add(img);
		layerDest.draw();		
	};
	imageObj.src = "css/img/dest-roleta.png";

	var balao = new Kinetic.Group({
		id: "balao",
		x: 0,
		y: 0,
		scaleX: 0,
		scaleY: 0
	})

	var vetor = new Kinetic.Path({
		id: "vetor",
		x: -rW/2 +2,
		y: -52,
		data: 'M185.05 2.9Q187.95 5.85 187.95 10L187.95 39.3Q187.95 43.45 185.05 46.4 182.1 49.3 177.95 49.3L96.75 49.3 91.65 54.95 86.55 49.3 10 49.3Q5.85 49.3 2.9 46.4 0 43.45 0 39.3L0 10Q0 5.85 2.9 2.9 5.85 0 10 0L177.95 0Q182.1 0 185.05 2.9Z',
		fill: '#69C4F1'
	});
	balao.add(vetor);

	var text = new Kinetic.Text({
        x: -rW/2 +2,
        y: -47,
        width: rW,
        height: 30,
        align:"center",
        fontFamily: 'solexbolditalicregular',
        fontSize: 17,
        text: "Agora você está mais para:",
        fill: '#FFF',
        shadowColor: "#000",
        shadowOpacity: .4,
        shadowOffsetY: -1,
        shadowBlur:0
    });
    balao.add(text);

    var rest = new Kinetic.Text({
        id:"resp-dest",
        x: -rW/2 +2,
        y: -28,
        width: rW,
        height: 30,
        align:"center",
        fontFamily: 'solexbolditalicregular',
        fontSize: 21,
        text: "Centralizador",
        fill: '#FFF',
        shadowColor: "#000",
        shadowOpacity: .4,
        shadowOffsetY: -1,
        shadowBlur:0
    });
    balao.add(rest);

    var ico;
	var imageIcoObj = new Image();
	imageIcoObj.onload = function() {
		ico = new Kinetic.Image({
			id:"ico-dest",
			image: imageIcoObj,
			x:-26,
			y:3
		});
		layerDest.add(ico);
		ico.setZIndex(2);
		layerDest.add(balao);
		layerDest.draw();
		if(Personalidade.resultado){
			obj.Atualizar();
		}
	};
	imageIcoObj.src = "css/img/ico-1.png";
	
	obj.stage.add(layer, layerDest);
}

objRoleta.prototype.Atualizar = function(){
	var obj = this;
	var layerDest = obj.stage.find("#layer-dest");
	var ico = obj.stage.find("#ico-dest")[0];
	var txt_balao = obj.stage.find("#resp-dest")[0];
	var balao = obj.stage.find("#balao")[0];

	txt_balao.text(arrPersonalidade[Personalidade.resultado - 1].name);

	var tb = new Kinetic.Tween({
		duration:.4,
		scaleX:.9,
		scaleY:.9,
		node:balao,
		easing: Kinetic.Easings.BackEaseOut
	})

	var t = new Kinetic.Tween({
		node: ico, 
		duration: .4,
		opacity: 1,
		easing: Kinetic.Easings.EaseIn
	});

	var imageIcoObj = new Image();
	imageIcoObj.onload = function() {
		ico.image(imageIcoObj)
		ico.setZIndex(2);
		layerDest.draw();
		t.play();
		tb.play();
	};

	Quiz.personalidade = Personalidade.resultado;
	imageIcoObj.src = "css/img/ico-"+Personalidade.resultado+".png";
}

// ICO
var objIco = function(id){
	this.id = id;
	this.ativo = false; 
	this.tween;
	this.intervalo = 360 / arrPersonalidade.length;
}
objIco.prototype.Renderizar = function(){
	var obj = this;
	
	var grupo = new Kinetic.Group({
		id:"grupo-ico-"+obj.id,
		scaleX:.55,
		scaleY:.55
	});

	var img;
	var imageObj = new Image();
	imageObj.onload = function() {
		img = new Kinetic.Image({
			image: imageObj,
			x:-26,
			y:-Quiz.roleta.stage.find('#layer-ico')[0].width() + 25
		});
		grupo.add(img);
		grupo.rotation(obj.id * obj.intervalo);
		grupo.draw(); 
	};
	imageObj.src = "css/img/ico-" + arrPersonalidade[obj.id].id + '.png';

	return grupo;
}
objIco.prototype.Ativar = function(){
	var obj = this;
	var icoDest = Quiz.roleta.stage.find("#ico-dest")[0];
	var layer = Quiz.roleta.stage.find('#layer-ico')[0];
	var balao = Quiz.roleta.stage.find('#balao')[0];
	var anglo = (obj.id) * -this.intervalo;
	
	if((obj.id + 1) != Quiz.personalidade){	
		var tb = new Kinetic.Tween({
			duration:.4,
			scaleX:0,
			scaleY:0,
			node:balao,
			easing: Kinetic.Easings.BackEaseIn
		})
		tb.play();

		var td = new Kinetic.Tween({
			node: icoDest, 
			duration: .4,
			opacity: 0,
			easing: Kinetic.Easings.EaseOut
		}).play();
		
		var t = new Kinetic.Tween({
			node: layer, 
			duration: .4,
			rotation: anglo,
			easing: Kinetic.Easings.EaseOut,
			onFinish: function(){
				Quiz.roleta.Atualizar();
			}
		}).play();
	}
}


//FINAL
var objFinal = function(){

}
objFinal.prototype.Renderizar = function(){
	var objTela = Navegador.arrTelas[2];
	var elm = objTela.elm;
	var obj = this;
	var i = Personalidade.resultado - 1;

	//renderiza informações na template
	elm.find("BLOCKQUOTE P STRONG SPAN").css("background-image", "url(css/img/ico-"+Personalidade.resultado+".png)");

	var html = elm.html()
	html = html.replace(/\$\{perfil\}/g, arrPersonalidade[i].name);
	html = html.replace(/\$\{descricao\}/g, arrPersonalidade[i].short_description);	
	elm.html(html);

	//renderiza o conteudo de profissoes de acordo com o perfil
	var arr_p = arrPersonalidade[i].professions;
	var elm_p = elm.find("DIV BLOCKQUOTE UL").eq(1);
	var html_p = "";
	for(j=0; j<arr_p.length; j++){
		html_p += "<li>" + (j+1) + "</li>";
	}

	html_p += "<a></a>"
	elm_p.html(html_p);

	//adiciona os cliques nos bullets
	elm_p.find("LI").each(function(index) {
	 	$(this).off().click(function(e){
			obj.trocaProfissao(index);
		})
	});

	//adiciona evento no holder
	elm_p.find("A").draggable({
		axis:"x",
		containment:"parent",
		stop: function(e,ui){
			var pos = $(this).position().left;
			var limite = arr_p.length;
			var minimo = 112;
			var intervalo = 36;
			var destino = 0;
			var index = 0;

			for(j=0; j<limite; j++){
				var atual = minimo + (j * intervalo) - (intervalo/2);
				if(atual <= pos){
					destino = atual + (intervalo/2);
					index = j;
				}
			}

			TweenLite.to(elm.find("DIV BLOCKQUOTE UL A"), .2, {left:destino + "px", ease:Linear.easeOut, onComplete:function(){
				obj.trocaProfissao(index);	
			}});
		}
	});

	//adiciona evento nos botoes das redes sociais
	var elm_s = elm.find("DIV BLOCKQUOTE").eq(1);
	
	var bt_facebook = elm_s.find("A").eq(0);
	bt_facebook.off().click(function(e){

		//FB.login(function(){
			var msg = "description=" + encodeURIComponent("Fiz o teste profissional Máquina de Profissões, do Guia do Estudante e descobri que o meu perfil é "+arrPersonalidade[i].name+". Quer saber qual é o seu? Faça o teste aqui");
			var link = "&link="+encodeURIComponent("http://guiadoestudante.abril.com.br/maquina-de-profissoes/");
			var app_id = "&app_id=310081332529394";
			var redirect_uri = "&redirect_uri="+encodeURIComponent("http://guiadoestudante.abril.com.br/maquina-de-profissoes/");
			var picture = "&picture=" + encodeURIComponent("http://guiadoestudante.abril.com.br/maquina-de-profissoes/css/img/share_icon.png");

			var url = "https://www.facebook.com/dialog/feed?" + msg + link + app_id + redirect_uri;
			var width = 530;
			var height = 450;
			var left = (screen.width/2)-(width/2);
	  		var top = (screen.height/2)-(height/2);

			window.location = url;
		//}, {scope: 'publish_actions'});

		track('["_trackEvent", "Resultado Deslogado", "Compartilhou", "Facebook"]');
	});

	var bt_twitter = elm_s.find("A").eq(1);
	bt_twitter.off().click(function(e){
		var msg = "text=" + encodeURIComponent("Meu perfil profissional é "+arrPersonalidade[i].name+". Quer saber qual é o seu? Faça o teste aqui:");
		var original_referer = "&original_referer="+encodeURIComponent("http://guiadoestudante.abril.com.br/maquina-de-profissoes/");
		var tw_p = "&tw_p=tweetbutton";
		var turl = "&url="+encodeURIComponent("http://guiadoestudante.abril.com.br/maquina-de-profissoes/");
		var via = "&via=guiadoestudante";

		var url = "https://twitter.com/share?" + msg + original_referer + tw_p + turl + via;

		var width = 530;
		var height = 450;
		var left = (screen.width/2)-(width/2);
  		var top = (screen.height/2)-(height/2);

		window.open(url, "twitter", "width="+width+", height="+height+", top="+top+",left="+left);
		track('["_trackEvent", "Resultado Deslogado", "Compartilhou", "Twitter"]');
	});


	//renderiza na tela os primeiros valores de Famosos e Profissoes
	obj.trocaFamoso(0);
	obj.trocaProfissao(0);

	//renderiza as pops
	elm.find("ASIDE A").off().click(function(e){
		$(".pop").find("STRONG").html(arrPersonalidade[i].name);
		
		$(".pop").find("ARTICLE #mCSB_1_container")
		.html(arrPersonalidade[i].long_description)
		.css("top", 0);

		$(".pop").find("ARTICLE .mCSB_dragger")
		.css("top", 0);

		openPop(".pop");

		track('["_trackEvent", "Resultado Deslogado", "Clicou para saber mais sobre perfil"]');
	})

	track('["_trackPageview", "resultado/deslogado"]');
}
objFinal.prototype.trocaFamoso = function(j){
	var objTela = Navegador.arrTelas[2];
	var elm = objTela.elm;
	var obj = this;
	var i = Personalidade.resultado - 1;
	var arr_f = arrPersonalidade[i].celebrities;
	var elm_f = elm.find("ASIDE UL LI I");
	
	var result = $.grep(arr_famoso, function(e){
		return e.id == arr_f[j];
	});

	if(result.length == 0){
		//console.log("não encontrado");
	}
	else if(result.length == 1){
		elm_f.text(result[0].name);
	}
	else{
	  //console.log("vários resultados");
	}

	TweenLite.to(elm_f, .7, {left:"300px", delay:3, ease:Back.easeIn, onComplete:function(){
		j = (j >= arr_f.length)? 0 : j+1;
		elm_f.css("left", "-300px");
		obj.trocaFamoso(j);
		TweenLite.to(elm_f, 1, {left:"0px", ease:Quint.easeOut});
	}});
}
objFinal.prototype.trocaProfissao = function(j){
	var objTela = Navegador.arrTelas[2];
	var elm = objTela.elm;
	var obj = this;
	var i = Personalidade.resultado - 1;
	var arr_p = arrPersonalidade[i].professions;
	var elm_p = elm.find("DIV BLOCKQUOTE UL LI");
	
	var result = $.grep(arr_profissao, function(e){
		return e.id == arr_p[j];
	});

	if(result.length == 0){
		//console.log("não encontrado");
	}
	else if(result.length == 1){
		TweenLite.to(elm_p.eq(0), .3, {right:"0px", ease:Quint.easeOut, onComplete:function(){
			elm_p.eq(0).text( j + 1 + ".");
			elm_p.eq(1).text(result[0].name);
			TweenLite.to(elm_p.eq(0), .5, {right:"245px", ease:Back.easeOut});
		}});

		var X = (112 + (j * 36)) + "px";
		TweenLite.to(elm.find("DIV BLOCKQUOTE UL A"), .5, {left:X, ease:Linear.easeOut});

		elm.find("DIV BLOCKQUOTE > A").eq(0).off().click(function(){
			window.open(result[0].url);
			track('["_trackEvent", "Resultado Deslogado", "Clicou para saber mais sobre a profissão"]');
		})
	}
	else{
		//console.log("vários resultados");
	}
}


//metodos auxiliares
function circuloCromatico(pa, tt, iv){
	//tt = total de posicoes
	//pa = posicao atual
	//iv = posicao do vermelho
	//no circulo cromatico a cor primaria esta presente em 4/6 da escala, sendo 100% em 2/6

	var sexto = tt/6;
	var fracao = 100/sexto;//intervalo de incremento/decremento da cor 

	this.range = function(v){
		v = (v > tt) ? v-tt : v;
		v = (v < 0) ? v+tt : v;
		return v
	}

	pa = this.range(pa);//enquadra o valor no range
	iv = (iv) ? iv : 0;//define um valor padrão caso não especificado;
	iv = iv + 2 * sexto;

	var pR = this.range(pa + iv);
	var pG = this.range(pa + iv + (2*sexto));
	var pB = this.range(pa + iv + (4*sexto));

	this.fn = function(pr){
		var cor = 0;
		if(pr < sexto){//cor presente em estado crescente na posicao
			cor = pr*fracao;
		}
		else if(pr >= sexto && pr <= (3*sexto)){//100% da cor presente na posicao
			cor = 100;
		}
		else if(pr > (3*sexto) && pr < (4*sexto)){//cor presente em estado decrescente na posicao
			cor = 100 - (pr-(3*sexto))*fracao;
		}
		return cor;
	}

	return "rgb("+this.fn(pR)+"%,"+this.fn(pG)+"%,"+this.fn(pB)+"%)";
}

function openPop(selector){
	var pop = $(selector);
	pop
		.css({opacity:0, top:((645 - pop.height()) / 2) + "px", left: ((screen.width - pop.width()) / 2) + "px"})
		.show();

	$("BODY > .bg_pop").css("opacity", 0).show();	
	TweenLite.to(pop.selector, .5, {autoAlpha:"1", ease:Quint.easeOut});
	TweenLite.to($("BODY > .bg_pop").selector, .5, {autoAlpha:".4", ease:Quint.easeOut});
}

function closePop(selector){
	var pop = $(selector);
	Navegador.clique = false;

	TweenLite.to(pop.selector, .5, {autoAlpha:"0", ease:Quint.easeOut, ease:Quint.easeOut});
	TweenLite.to($("BODY > .bg_pop").selector, .5, {autoAlpha:"0"});

	TweenLite.delayedCall(.5, function(){
		$("BODY > .pop").hide();
		$("BODY > .bg_pop").hide();
		Navegador.clique = true;
	})
}

var Navegador;
var Quiz;
var Personalidade;
var Marcador;
var Frase;
var Final;

var Iniciar = function(){

	//atribui os metodos aos eventos (handlers)
	$(document).bind("onOpen0", montaAbertura);
	$(document).bind("onClose0", desmontaAbertura);

	$(document).bind("onOpen1", montaQuiz);
	$(document).bind("onClose1", desmontaQuiz);

	$(document).bind("onOpen2", montaFinal);
	$(document).bind("onClose2", desmontaFinal);


	$("#pop > A:nth-child(1), #pop > A:nth-child(5)").click(function(){
		closePop("#pop");
	});

	Navegador = new objNavegador();
	Navegador.goTo(0);
	Quiz = new objQuiz();
	Personalidade = new objPersonalidade();
	Marcador = new objMarcador();
	Frase = new objFrase();
	Final = new objFinal();

	//
	track('["_trackPageview", "home/abriu"]');
}

$(document).ready(function() {
    //diferencia escala entre tablets e celulares 
    var vpw;
    var initial_scale;
    var maximum_scale;

    if(screen.width>=768){
    	vpw = '980';
    	initial_scale = "1";
    	maximum_scale = "1";
    }
    else{
    	vpw = 'device-width';
    	initial_scale = ".4";
    	maximum_scale = ".8";
    }

    $('head').prepend('<meta name="viewport" content="initial-scale='+initial_scale+', maximum-scale='+maximum_scale+', user-scalable=1" />');
    
    //Preloader imagens de icones

    var count = 0;
    for(i=1; i<arrPersonalidade.length; i++){
    	var imageIcoObj = new Image();
    	imageIcoObj.onload = function(){
    		count ++;
    		if(count == arrPersonalidade.length-1){
    			Iniciar();
    		}
    	}
    	imageIcoObj.src = "css/img/ico-"+i+".png";
    }

    //inicia plugin
	$.mCustomScrollbar.defaults.scrollButtons.enable=false; //enable scrolling buttons by default
	$.mCustomScrollbar.defaults.autoDraggerLength=false;
	$.mCustomScrollbar.defaults.scrollbarPosition="outside";
	$.mCustomScrollbar.defaults.axis="y"; //enable 2 axis scrollbars by default
	$(".pop ARTICLE").mCustomScrollbar();
	
	//
	track('["_trackPageview", "home/carregando"]');
});

